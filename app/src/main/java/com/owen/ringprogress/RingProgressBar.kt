package com.owen.ringprogress

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import kotlin.math.min

class RingProgressBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val textPaint = Paint().apply {
        this.isAntiAlias = true
        style = Paint.Style.FILL
    }
    private val paint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }
    private val bgPaint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }

    private var rectF: RectF = RectF()
    private var textRect: Rect = Rect()

    private var radiusOffset = 0f
    var progressValue: Int = 10
        set(value) {
            field = value
            invalidate()
        }

    var max = 100f

    private var centerW = 0f
    private var centerH = 0f
    private var radius = 0f

    init {
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.RingProgressBar)
            with(bgPaint) {
                strokeWidth = ta.getDimension(R.styleable.RingProgressBar_bgRingSize, 20f)
                color = ta.getColor(R.styleable.RingProgressBar_bgRingColor, Color.LTGRAY)
            }

            with(paint) {
                color = ta.getColor(
                    R.styleable.RingProgressBar_pRingColor,
                    resources.getColor(R.color.green)
                )
                strokeWidth = ta.getDimension(R.styleable.RingProgressBar_pRingSize, 40f)
            }

            with(textPaint) {
                color = ta.getColor(
                    R.styleable.RingProgressBar_textColor,
                    resources.getColor(R.color.green)
                )
                textSize = ta.getDimension(
                    R.styleable.RingProgressBar_textSize,
                    TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_SP,
                        30f,
                        context.resources.displayMetrics
                    )
                )
            }

            radiusOffset = ta.getDimension(R.styleable.RingProgressBar_RingPadding, 20f)
            progressValue = ta.getInteger(R.styleable.RingProgressBar_progressValue, 0)
            ta.recycle()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        Log.d("RingProgressBar", "onSizeChanged")
        centerW = w / 2f
        centerH = h / 2f
        radius = min(centerW, centerH) - radiusOffset

        rectF.apply {
            left = radiusOffset
            top = h / 2 - radius
            right = w - radiusOffset
            bottom = h / 2 + radius
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        Log.d("RingProgressBar", "onDraw")

        with(canvas) {
            // add bg
            drawCircle(centerW, centerH, radius, bgPaint)

            //Counter clock
            val startArc = (1 - progressValue.toFloat() / max) * 359f
            drawArc(rectF, 270f + startArc, 359f - startArc, false, paint)

            // progress text
            val text = "$progressValue%"
            textPaint.getTextBounds(text, 0, text.length, textRect)
            val posX = width / 2 - textRect.width() / 2f - textRect.left
            val posY = height / 2 - textRect.height() / 2f - textRect.top
            drawText(text, posX, posY, textPaint)
        }
    }
}