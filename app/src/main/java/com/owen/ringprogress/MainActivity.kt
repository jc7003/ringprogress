package com.owen.ringprogress

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private val handler = Handler()

    private var progressRunnable = object : Runnable {

        override fun run() {
            progress ?: return
            progress?.run {
                if (progressValue < max) {
                    progressValue += 1
                }
            }
            handler.postDelayed(this, 100)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        handler.post(progressRunnable)
    }

    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(progressRunnable)
    }
}
